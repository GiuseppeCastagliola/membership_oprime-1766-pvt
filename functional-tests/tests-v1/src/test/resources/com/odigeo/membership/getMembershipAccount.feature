Feature: Test getMembershipAccount

  Scenario Outline: Check MembershipAccountInfo
    Given the next memberAccount stored in db:
      | memberAccountId | userId   | firstName   | lastNames  | timestamp   |
      | <id>            | <userId> | <firstName> | <lastName> | <timestamp> |
    When the client request getMembershipAccount to membership API with membershipAccountId <id>
    Then the membershipAccount response is:
      | id   | userId   | firstName   | lastName   | timestamp   |
      | <id> | <userId> | <firstName> | <lastName> | <timestamp> |
    Examples:
      | id  | userId | firstName | lastName | timestamp           |
      | 91  | 1010   | SQUALL    | LEONHART | 2019-11-11T16:15:14 |
      | 894 | 1894   | JON       | SMITH    | 2015-10-11T01:06:09 |
