Feature: Test getUser service

  Background:
    Given the next memberAccount stored in db:
      | memberAccountId | userId | firstName | lastNames |
      | 123             | 4321   | JOSE      | GARCIA    |
      | 321             | 1234   | ANA       | JUNYENT   |
      | 555             | 1555   | MARIO     | GOMEZ     |
      | 678             | 1999   | LUIS      | GONZALEZ  |
      | 399             | 3333   | SERGIO    | LOPEZ     |
      | 400             | 3333   | SERGIO    | LOPEZ     |

    And the next membership stored in db:
      | memberId | website     | status              | autoRenewal | memberAccountId | activationDate | expirationDate |
      | 123      | ES          | ACTIVATED           | ENABLED     | 123             | 2017-07-06     | 2018-07-06     |
      | 321      | ES          | PENDING_TO_ACTIVATE | ENABLED     | 321             | 2017-07-06     | 2018-07-06     |
      | 322      | ES          | EXPIRED             | DISABLED    | 321             | 2016-07-06     | 2017-07-06     |
      | 323      | IT          | EXPIRED             | DISABLED    | 321             | 2016-07-06     | 2017-07-06     |
      | 987      | ES          | EXPIRED             | ENABLED     | 555             | 2017-11-11     | 2018-11-11     |
      | 989      | ES          | DEACTIVATED         | ENABLED     | 555             | 2016-11-11     | 2017-11-11     |
      | 678      | ES          | ACTIVATED           | ENABLED     | 678             | 2018-08-06     | 2019-08-06     |
      | 679      | ES          | EXPIRED             | ENABLED     | 678             | 2017-08-06     | 2018-08-06     |
      | 380      | IT          | EXPIRED             | ENABLED     | 399             | 2017-08-06     | 2018-08-06     |
      | 381      | IT          | ACTIVATED           | DISABLED    | 400             | 2018-08-06     | 2019-08-06     |
      | 382      | FR          | ACTIVATED           | DISABLED    | 401             | 2018-09-06     | 2019-09-06     |

  # 1 - Positive test
  Scenario Outline: Check correct userId is returned
    When the userId for member with membershipId <membershipId> is requested
    Then the userId <userId> is returned

    Examples:
      | membershipId | userId |
      | 123          | 4321   |
      | 321          | 1234   |
      | 322          | 1234   |
      | 987          | 1555   |
      | 989          | 1555   |

  # 2 - Negative test
  Scenario Outline: Check error is returned for invalid membershipId
    When the userId for member with invalid membershipId <membershipId> is requested
    Then Exceptions occurs

    Examples:
      | membershipId |
      | 382          |
      | 500          |

