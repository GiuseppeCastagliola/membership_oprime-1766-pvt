Feature: Test save partial product service

  Background:
    Given the next membership product stored in db:
      | memberId | website | status             | autoRenewal | memberAccountId | activationDate | expirationDate | sourceType     | monthsDuration | productStatus |
      | 123      | ES      | PENDING_TO_COLLECT | ENABLED     | 123             | 2017-07-06     | 2018-07-06     | FUNNEL_BOOKING | 12             | INIT          |

  Scenario: Checking that save partial service is not modifying product and membership status
    When save partial information for product id 123
    Then the product and membership status for product id 123 are:
      | membershipStatus   | productStatus |
      | PENDING_TO_COLLECT | INIT          |
