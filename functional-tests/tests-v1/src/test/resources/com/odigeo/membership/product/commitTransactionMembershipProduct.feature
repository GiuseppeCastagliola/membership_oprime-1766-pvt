Feature: Test commit transaction product service

  Background:
    Given the next memberAccount stored in db:
      | memberAccountId | userId | firstName | lastNames   |
      | 9938            | 1010   | JOSE      | GARCIA      |
      | 1991            | 1028   | LEVI      | ACKERMAN    |
    And the next membership product stored in db:
      | memberId | website     | status             | autoRenewal | memberAccountId | activationDate | expirationDate | balance | sourceType     | monthsDuration | productStatus | totalPrice |
      | 123      | ES          | PENDING_TO_COLLECT | ENABLED     | 9938            | 2017-07-06     | 2018-07-06     | 54.99   | FUNNEL_BOOKING | 12             | INIT          | 60.0       |
      | 128      | CH          | PENDING_TO_COLLECT | ENABLED     | 1991            | 2021-01-01     | 2891-02-08     |  4.99   | FUNNEL_BOOKING | 1              | INIT          | 4.99       |
    And the user-api has the following user info
      | userId | status        | email           | brand | tokenType        |
      | 1010   | PENDING_LOGIN | test@odigeo.com | ED    | REQUEST_PASSWORD |
      | 1028   | PENDING_LOGIN | levi@odigeo.com | ED    | REQUEST_PASSWORD |

  Scenario: Checking that commit transaction service modifies product status, activates the membership and sets the activation date and the balance.
    When commit transaction for product id 123
    Then the product and membership status for product id 123 are:
      | membershipStatus    | productStatus | balance | activationDate |
      | ACTIVATED           | CONTRACT      | 60.0    | today          |

  Scenario: Saving partially a PTC with monthly subscription
    When commit transaction for product id 128
    Then the product and membership status for product id 128 are:
      | membershipStatus  | productStatus | balance | activationDate | expirationDate |
      | ACTIVATED         | CONTRACT      | 4.99    | today          | nextMonth      |
