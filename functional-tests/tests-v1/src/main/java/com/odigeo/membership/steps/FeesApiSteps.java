package com.odigeo.membership.steps;

import com.google.inject.Inject;
import com.odigeo.fees.model.AbstractFee;
import com.odigeo.fees.model.FeeContainer;
import com.odigeo.fees.model.FeeContainerType;
import com.odigeo.fees.model.FeeLabel;
import com.odigeo.fees.model.FixFee;
import com.odigeo.membership.mocks.feesapi.FeesApiWorld;
import cucumber.api.DataTable;
import cucumber.api.java.en.Given;

import java.math.BigDecimal;
import java.util.Collections;
import java.util.Currency;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class FeesApiSteps {

    private static final String ID_COLUMN_NAME = "Id";
    private static final String FEE_CONTAINER_TYPE_COLUMN_NAME = "FeeContainerType";
    private static final String AMOUNT_COLUMN_NAME = "Amount";
    private static final String FEE_LABEL_COLUMN_NAME = "FeeLabel";
    private static final String CURRENCY_COLUMN_NAME = "Currency";
    private static final String SUB_CODE_COLUMN_NAME = "SubCode";
    private final FeesApiWorld feesApiWorld;

    @Inject
    public FeesApiSteps(FeesApiWorld feesApiWorld) {
        this.feesApiWorld = feesApiWorld;
    }

    @Given("^the fees-api has the following fee containers:$")
    public void givenFeesContainer(DataTable dataTable) {
        List<Map<String, String>> feeContainers = dataTable.asMaps(String.class, String.class);
        feeContainers.forEach(this::addFeeContainer);
    }

    private void addFeeContainer(Map<String, String> feeContainerAsMap) {
        FeeContainer feeContainer = new FeeContainer();
        feeContainer.setId(Long.valueOf(feeContainerAsMap.get(ID_COLUMN_NAME)));
        feeContainer.setFeeContainerType(FeeContainerType.valueOf(feeContainerAsMap.get(FEE_CONTAINER_TYPE_COLUMN_NAME)));
        HashMap<FeeLabel, List<AbstractFee>> feeMap = new HashMap<>();
        FixFee fixFee = new FixFee();
        fixFee.setAmount(new BigDecimal(feeContainerAsMap.get(AMOUNT_COLUMN_NAME)));
        fixFee.setFeeLabel(FeeLabel.valueOf(feeContainerAsMap.get(FEE_LABEL_COLUMN_NAME)));
        fixFee.setCurrency(Currency.getInstance(feeContainerAsMap.get(CURRENCY_COLUMN_NAME)));
        fixFee.setSubCode(feeContainerAsMap.get(SUB_CODE_COLUMN_NAME));
        feeMap.put(FeeLabel.valueOf(feeContainerAsMap.get(FEE_LABEL_COLUMN_NAME)), Collections.singletonList(fixFee));
        feeContainer.setFeeMap(feeMap);
        feesApiWorld.getFeesServiceMock().addFeeContainer(feeContainer, feeContainer.getId());
    }
}
