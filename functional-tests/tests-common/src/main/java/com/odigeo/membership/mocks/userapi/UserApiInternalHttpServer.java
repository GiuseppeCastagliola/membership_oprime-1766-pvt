package com.odigeo.membership.mocks.userapi;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.odigeo.membership.server.JaxRsServiceHttpServer;

@Singleton
public class UserApiInternalHttpServer extends JaxRsServiceHttpServer {

    private static final String USER_API_PATH = "/user-profiles";
    private static final int USER_API_PORT = 56000;

    @Inject
    public UserApiInternalHttpServer() {
        super(USER_API_PATH, USER_API_PORT);
    }
}
