package com.odigeo.membership.functionals.membership;

import java.time.Instant;
import java.util.Objects;

public class AuditMemberAccountBuilder {

    private Long memberAccountId;
    private Long userId;
    private String firstName;
    private String lastName;
    private Instant timestamp;

    public Long getMemberAccountId() {
        return memberAccountId;
    }

    public AuditMemberAccountBuilder setMemberAccountId(final Long memberAccountId) {
        this.memberAccountId = memberAccountId;
        return this;
    }

    public Long getUserId() {
        return userId;
    }

    public AuditMemberAccountBuilder setUserId(final Long userId) {
        this.userId = userId;
        return this;
    }

    public String getFirstName() {
        return firstName;
    }

    public AuditMemberAccountBuilder setFirstName(final String firstName) {
        this.firstName = firstName;
        return this;
    }

    public String getLastName() {
        return lastName;
    }

    public AuditMemberAccountBuilder setLastName(final String lastName) {
        this.lastName = lastName;
        return this;
    }

    public Instant getTimestamp() {
        return timestamp;
    }

    public AuditMemberAccountBuilder setTimestamp(final Instant timestamp) {
        this.timestamp = timestamp;
        return this;
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final AuditMemberAccountBuilder that = (AuditMemberAccountBuilder) o;
        return Objects.equals(memberAccountId, that.memberAccountId)
                && Objects.equals(userId, that.userId)
                && Objects.equals(firstName, that.firstName)
                && Objects.equals(lastName, that.lastName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(memberAccountId, userId, firstName, lastName);
    }

    @Override
    public String toString() {
        return "AuditMemberAccountBuilder{"
                + "memberAccountId=" + memberAccountId
                + ", userId=" + userId
                + ", firstName='" + firstName + '\''
                + ", lastName='" + lastName + '\''
                + ", timestamp=" + timestamp
                + '}';
    }
}
