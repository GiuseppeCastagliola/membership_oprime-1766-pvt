package com.odigeo.product.membership.service.util;

import com.odigeo.membership.MemberStatus;
import com.odigeo.membership.Membership;
import com.odigeo.membership.MembershipBuilder;
import com.odigeo.membership.MembershipRenewal;
import com.odigeo.product.response.BookingApiMembershipInfo;
import com.odigeo.product.v2.model.Fee;
import com.odigeo.product.v2.model.Product;
import com.odigeo.product.v2.model.enums.CloseActionResult;
import com.odigeo.product.v2.model.enums.ProductStatus;
import com.odigeo.product.v2.model.enums.ProviderProductStatus;
import com.odigeo.product.v2.model.responses.CloseProviderProductResponse;
import com.odigeo.util.FeeMapper;
import org.testng.annotations.Test;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static java.time.LocalDateTime.now;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNotNull;
import static org.testng.Assert.assertNull;
import static org.testng.Assert.assertTrue;

public class ProductServiceMappingV2Test {

    private static final String WEBSITE = "ES";
    private static final String MEMBERSHIP_ID = "321";
    private static final long MEMBER_ACCOUNT_ID = 123L;
    private static final LocalDateTime TODAY = now();
    private static final LocalDateTime NEXT_YEAR = TODAY.plusYears(1);

    private static final Membership MEMBERSHIP = new MembershipBuilder().setId(Long.parseLong(MEMBERSHIP_ID)).setWebsite(WEBSITE).setStatus(MemberStatus.ACTIVATED).setMembershipRenewal(MembershipRenewal.ENABLED).setActivationDate(TODAY).setExpirationDate(NEXT_YEAR).setMemberAccountId(MEMBER_ACCOUNT_ID).setProductStatus(com.odigeo.membership.ProductStatus.CONTRACT).build();
    private static final Membership MEMBERSHIP_NULL_EXP_DATE = new MembershipBuilder().setId(Long.parseLong(MEMBERSHIP_ID)).setWebsite(WEBSITE).setStatus(MemberStatus.ACTIVATED).setMembershipRenewal(MembershipRenewal.ENABLED).setActivationDate(TODAY).setExpirationDate(null).setMemberAccountId(MEMBER_ACCOUNT_ID).setProductStatus(com.odigeo.membership.ProductStatus.CONTRACT).build();
    private static final Membership MEMBERSHIP_NON_ACTIVE = new MembershipBuilder().setId(Long.parseLong(MEMBERSHIP_ID)).setWebsite(WEBSITE).setStatus(MemberStatus.DEACTIVATED).setMembershipRenewal(MembershipRenewal.ENABLED).setActivationDate(TODAY).setExpirationDate(NEXT_YEAR).setMemberAccountId(MEMBER_ACCOUNT_ID).setProductStatus(com.odigeo.membership.ProductStatus.INIT).build();
    private static final long FEE_ID = 2L;
    private static final String FEE_LABEL = "FEE_LABEL";

    @Test
    public void testAsProduct() {
        Product product = ProductServiceMappingV2.asProduct(MEMBERSHIP, mockFees());
        assertNotNull(product);
        assertEquals(product.getId(), MEMBERSHIP_ID);
        assertEquals(product.getStatus(), ProductStatus.CONTRACT.toString());
    }

    @Test
    public void testAsProductInit() {
        Product product = ProductServiceMappingV2.asProduct(MEMBERSHIP_NON_ACTIVE, mockFees());
        assertNotNull(product);
        assertEquals(product.getStatus(), ProductStatus.INIT.toString());
    }

    @Test
    public void testAsBookingApiMembershipInfo() {
        BookingApiMembershipInfo bookingApiMembershipInfo = ProductServiceMappingV2.asBookingApiMembershipInfo(MEMBERSHIP, mockFees());
        assertNotNull(bookingApiMembershipInfo);
        assertTrue(bookingApiMembershipInfo.isRenewal());
    }

    @Test
    public void testAsBookingApiMembershipInfoNullExpDate() {
        BookingApiMembershipInfo bookingApiMembershipInfo = ProductServiceMappingV2.asBookingApiMembershipInfo(MEMBERSHIP_NULL_EXP_DATE, mockFees());
        assertNotNull(bookingApiMembershipInfo);
        assertTrue(bookingApiMembershipInfo.isRenewal());
        assertNull(bookingApiMembershipInfo.getRenewalDate());
    }

    @Test
    public void testAsCloseProviderProductResponseContract() {
        CloseProviderProductResponse closeProviderProductResponse = ProductServiceMappingV2.asCloseProviderProductResponse(ProviderProductStatus.CONTRACT);
        assertEquals(ProviderProductStatus.CONTRACT, closeProviderProductResponse.getProviderProductStatus());
        assertEquals(CloseActionResult.OK, closeProviderProductResponse.getCloseActionResult());
    }

    @Test
    public void testAsCloseProviderProductResponseNotContract() {
        CloseProviderProductResponse closeProviderProductResponse = ProductServiceMappingV2.asCloseProviderProductResponse(ProviderProductStatus.INIT);
        assertEquals(ProviderProductStatus.INIT, closeProviderProductResponse.getProviderProductStatus());
        assertEquals(CloseActionResult.KO, closeProviderProductResponse.getCloseActionResult());
    }

    private List<Fee> mockFees() {
        ArrayList<Fee> fees = new ArrayList<>();
        Fee fee = new Fee();
        fee.setId(FEE_ID);
        fee.setCreationDate(new Date());
        fee.setLabel(FEE_LABEL);
        fee.setPrice(new FeeMapper().getPrice(BigDecimal.TEN, "USD"));
        fees.add(fee);
        return fees;
    }

}
