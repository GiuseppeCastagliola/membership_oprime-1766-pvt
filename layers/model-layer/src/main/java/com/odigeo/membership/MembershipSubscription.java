package com.odigeo.membership;

import java.io.Serializable;

public class MembershipSubscription implements Serializable {
    private Long productId;
    private String website;

    public MembershipSubscription(Long productId, String website) {
        this.productId = productId;
        this.website = website;
    }

    public Long getProductId() {
        return productId;
    }

    public void setProductId(Long productId) {
        this.productId = productId;
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    @Override
    public String toString() {
        return productId + "|" + website;
    }
}
