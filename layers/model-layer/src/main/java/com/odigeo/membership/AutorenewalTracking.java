package com.odigeo.membership;

import com.odigeo.commons.uuid.UUIDGenerator;
import org.apache.commons.lang.StringUtils;

import java.time.LocalDateTime;
import java.util.Locale;
import java.util.UUID;

public class AutorenewalTracking {

    private static final int STRING_START = 0;
    private static final int STRING_END = 25;
    private final UUID id = UUIDGenerator.getInstance().generateUUID();
    private String membershipId;
    private final LocalDateTime timestamp = LocalDateTime.now();
    private AutoRenewalOperation autoRenewalOperation;
    private String requestedMethod;
    private String requester;
    private Integer interfaceId;
    private String visitInformation;

    public void setMembershipId(String membershipId) {
        this.membershipId = membershipId;
    }

    public void setAutoRenewalOperation(AutoRenewalOperation autoRenewalOperation) {
        this.autoRenewalOperation = autoRenewalOperation;
    }

    public void setRequestedMethod(String requestedMethod) {
        this.requestedMethod = StringUtils.substring(requestedMethod, STRING_START, STRING_END).toUpperCase(Locale.getDefault());
    }

    public void setRequester(String requester) {
        this.requester = StringUtils.substring(requester, STRING_START, STRING_END).toUpperCase(Locale.getDefault());
    }

    public UUID getId() {
        return id;
    }

    public String getMembershipId() {
        return membershipId;
    }

    public LocalDateTime getTimestamp() {
        return timestamp;
    }

    public AutoRenewalOperation getAutoRenewalOperation() {
        return autoRenewalOperation;
    }

    public String getRequestedMethod() {
        return requestedMethod;
    }

    public String getRequester() {
        return requester;
    }

    public Integer getInterfaceId() {
        return interfaceId;
    }

    public void setInterfaceId(Integer interfaceId) {
        this.interfaceId = interfaceId;
    }

    public String getVisitInformation() {
        return visitInformation;
    }

    public void setVisitInformation(String visitInformation) {
        this.visitInformation = visitInformation;
    }
}
