package com.odigeo.membership.exception;

public class ExistingRecurringException extends Exception {

    public ExistingRecurringException(String message) {
        super(message);
    }
}
