package com.odigeo.membership.member;

import com.edreams.base.DataAccessException;
import com.edreams.configuration.ConfigurationEngine;
import com.odigeo.membership.MembershipFee;
import com.odigeo.membership.product.MembershipSubscriptionFeeStore;

import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import java.sql.SQLException;
import java.util.List;
import java.util.Objects;

@Stateless
@Local(MembershipProductService.class)
@TransactionAttribute(TransactionAttributeType.REQUIRED)
public class MembershipProductServiceBean extends AbstractServiceBean implements MembershipProductService {

    private MembershipSubscriptionFeeStore membershipSubscriptionFeeStore;

    @Override
    public List<MembershipFee> getMembershipFees(Long membershipId) throws DataAccessException {
        try {
            return getMembershipSubscriptionFeeStore().getMembershipFee(dataSource, String.valueOf(membershipId));
        } catch (SQLException e) {
            throw new DataAccessException("There was an error trying to retrieve the Membership Fees" + membershipId, e);
        }
    }

    private MembershipSubscriptionFeeStore getMembershipSubscriptionFeeStore() {
        if (Objects.isNull(membershipSubscriptionFeeStore)) {
            membershipSubscriptionFeeStore = ConfigurationEngine.getInstance(MembershipSubscriptionFeeStore.class);
        }
        return membershipSubscriptionFeeStore;
    }
}
