package com.odigeo.membership.member.renewal;

import com.edreams.base.DataAccessException;
import com.odigeo.membership.Membership;

import javax.sql.DataSource;

public interface MembershipRenewalService {

    boolean activatePendingToCollect(DataSource dataSource, Membership membership) throws DataAccessException;
}
