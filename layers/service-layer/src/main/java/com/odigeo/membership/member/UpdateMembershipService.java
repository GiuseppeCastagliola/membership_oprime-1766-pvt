package com.odigeo.membership.member;

import com.edreams.base.DataAccessException;
import com.edreams.base.MissingElementException;
import com.odigeo.membership.Membership;
import com.odigeo.membership.UpdateMembership;
import com.odigeo.membership.exception.ExistingRecurringException;

public interface UpdateMembershipService {

    Boolean updateMembership(UpdateMembership updateMembership) throws MissingElementException, DataAccessException, ExistingRecurringException;

    String disableAutoRenewal(UpdateMembership updateMembership) throws MissingElementException, DataAccessException;

    String enableAutoRenewal(UpdateMembership updateMembership) throws DataAccessException, MissingElementException;

    String deactivateMembership(UpdateMembership updateMembership) throws MissingElementException, DataAccessException;

    String reactivateMembership(UpdateMembership updateMembership) throws DataAccessException, MissingElementException;

    String expireMembership(UpdateMembership updateMembership) throws DataAccessException, MissingElementException;

    String discardMembership(UpdateMembership updateMembership) throws DataAccessException, MissingElementException;

    boolean consumeMembershipRemnantBalance(UpdateMembership updateMembership) throws DataAccessException;

    boolean activateRenewalPendingToCollect(Membership membership) throws DataAccessException;

    boolean setRemindMeLater(UpdateMembership updateMembership) throws DataAccessException;
}
