package com.odigeo.membership.auth.config;

import com.odigeo.authservices.authorization.api.v1.client.AuthServicesAuthorizationApiClientConfiguration;

public class AuthorizationServiceConfiguration extends AuthServicesAuthorizationApiClientConfiguration {

    public AuthorizationServiceConfiguration(final String url) {
        super();
        setUrl(url);
    }
}
