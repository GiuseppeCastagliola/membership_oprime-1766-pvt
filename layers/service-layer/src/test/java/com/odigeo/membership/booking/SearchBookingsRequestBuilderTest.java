package com.odigeo.membership.booking;

import com.odigeo.bookingsearchapi.v1.requests.SearchBookingsRequest;
import org.testng.annotations.Test;

import java.util.Calendar;
import java.util.Locale;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertTrue;

public class SearchBookingsRequestBuilderTest {

    private static final Calendar INITIAL_BOOKING_DATE = Calendar.getInstance(Locale.CHINA);
    private static final Calendar FINAL_BOOKING_DATE = Calendar.getInstance(Locale.CANADA);
    private static final String LOCATOR = "LOCATOR";
    private static final String BUYER_EMAIL = "BUYER_EMAIL";
    private static final String TRAVELLER_NAME = "TRAVELLER_NAME";
    private static final String CLIENT_BOOKING_REFERENCE_ID = "CLIENT_BOOKING_REFERENCE_ID";
    private static final Boolean INCLUDE_ORDERS = false;
    private static final String BUYER_PHONE_NUMBER = "653234567";
    private static final Calendar INITIAL_LAST_ARRIVAL_DATE = Calendar.getInstance(Locale.JAPAN);
    private static final Calendar FINAL_LAST_ARRIVAL_DATE = Calendar.getInstance(Locale.ITALIAN);
    private static final String CREDIT_CARD = "4213456709871234";
    private static final Integer OFFSET_PAGE = 0;
    private static final Integer MAX_NUM_BOOKINGS_BY_PAGE = 1;
    private static final String WEBSITE_CODE = "ES";
    private static final String BRAND_CODE = "GB";
    private static final Long MEMBERSHIP_ID = 8L;
    private static final Long MEMBER_ACCOUNT_ID = 91L;

    @Test
    public void testSearchBookingsRequestBuilder() {
        SearchBookingsRequestBuilder searchBookingsRequestBuilder = buildSearchBookingRequestBuilder();
        validateSearchBookingsRequestBuilder(searchBookingsRequestBuilder);
    }

    @Test
    public void testSearchBookingsRequestBuilderBuild() {
        SearchBookingsRequest searchBookingsRequest = buildSearchBookingRequestBuilder().build();
        validateSearchBookingsRequest(searchBookingsRequest);
    }

    private SearchBookingsRequestBuilder buildSearchBookingRequestBuilder() {
        SearchBookingsRequestBuilder searchBookingsRequestBuilder = new SearchBookingsRequestBuilder();
        searchBookingsRequestBuilder.setInitialBookingDate(INITIAL_BOOKING_DATE);
        searchBookingsRequestBuilder.setFinalBookingDate(FINAL_BOOKING_DATE);
        searchBookingsRequestBuilder.setLocator(LOCATOR);
        searchBookingsRequestBuilder.setBuyerEmail(BUYER_EMAIL);
        searchBookingsRequestBuilder.setTravellerName(TRAVELLER_NAME);
        searchBookingsRequestBuilder.setClientBookingReferenceId(CLIENT_BOOKING_REFERENCE_ID);
        searchBookingsRequestBuilder.setIncludeOrders(INCLUDE_ORDERS);
        searchBookingsRequestBuilder.setBuyerPhoneNumber(BUYER_PHONE_NUMBER);
        searchBookingsRequestBuilder.setInitialLastArrivalDate(INITIAL_LAST_ARRIVAL_DATE);
        searchBookingsRequestBuilder.setFinalLastArrivalDate(FINAL_LAST_ARRIVAL_DATE);
        searchBookingsRequestBuilder.setCreditCard(CREDIT_CARD);
        searchBookingsRequestBuilder.setOffsetPage(OFFSET_PAGE);
        searchBookingsRequestBuilder.setMaxNumBookingsByPage(MAX_NUM_BOOKINGS_BY_PAGE);
        searchBookingsRequestBuilder.setWebsiteCode(WEBSITE_CODE);
        searchBookingsRequestBuilder.setBrandCode(BRAND_CODE);
        searchBookingsRequestBuilder.setMembershipId(MEMBERSHIP_ID);
        searchBookingsRequestBuilder.setMemberAccountId(MEMBER_ACCOUNT_ID);
        searchBookingsRequestBuilder.setBookingSubscriptionPrime(Boolean.TRUE.toString());
        return searchBookingsRequestBuilder;
    }

    private void validateSearchBookingsRequestBuilder(SearchBookingsRequestBuilder searchBookingsRequestBuilder) {
        assertEquals(searchBookingsRequestBuilder.getInitialBookingDate(), INITIAL_BOOKING_DATE);
        assertEquals(searchBookingsRequestBuilder.getFinalBookingDate(), FINAL_BOOKING_DATE);
        assertEquals(searchBookingsRequestBuilder.getLocator(), LOCATOR);
        assertEquals(searchBookingsRequestBuilder.getBuyerEmail(), BUYER_EMAIL);
        assertEquals(searchBookingsRequestBuilder.getTravellerName(), TRAVELLER_NAME);
        assertEquals(searchBookingsRequestBuilder.getClientBookingReferenceId(), CLIENT_BOOKING_REFERENCE_ID);
        assertFalse(searchBookingsRequestBuilder.getIncludeOrders());
        assertEquals(searchBookingsRequestBuilder.getBuyerPhoneNumber(), BUYER_PHONE_NUMBER);
        assertEquals(searchBookingsRequestBuilder.getInitialLastArrivalDate(), INITIAL_LAST_ARRIVAL_DATE);
        assertEquals(searchBookingsRequestBuilder.getFinalLastArrivalDate(), FINAL_LAST_ARRIVAL_DATE);
        assertEquals(searchBookingsRequestBuilder.getCreditCard(), CREDIT_CARD);
        assertEquals(searchBookingsRequestBuilder.getOffsetPage(), OFFSET_PAGE);
        assertEquals(searchBookingsRequestBuilder.getMaxNumBookingsByPage(), MAX_NUM_BOOKINGS_BY_PAGE);
        assertEquals(searchBookingsRequestBuilder.getWebsiteCode(), WEBSITE_CODE);
        assertEquals(searchBookingsRequestBuilder.getBrandCode(), BRAND_CODE);
        assertEquals(searchBookingsRequestBuilder.getMembershipId(), MEMBERSHIP_ID);
        assertEquals(searchBookingsRequestBuilder.getMemberAccountId(), MEMBER_ACCOUNT_ID);
        assertTrue(Boolean.parseBoolean(searchBookingsRequestBuilder.getBookingSubscriptionPrime()));
    }

    private void validateSearchBookingsRequest(SearchBookingsRequest searchBookingsRequest) {
        assertEquals(searchBookingsRequest.getInitialBookingDate(), INITIAL_BOOKING_DATE);
        assertEquals(searchBookingsRequest.getFinalBookingDate(), FINAL_BOOKING_DATE);
        assertEquals(searchBookingsRequest.getLocator(), LOCATOR);
        assertEquals(searchBookingsRequest.getBuyerEmail(), BUYER_EMAIL);
        assertEquals(searchBookingsRequest.getTravellerName(), TRAVELLER_NAME);
        assertEquals(searchBookingsRequest.getClientBookingReferenceId(), CLIENT_BOOKING_REFERENCE_ID);
        assertFalse(searchBookingsRequest.isIncludeOrders());
        assertEquals(searchBookingsRequest.getBuyerPhoneNumber(), BUYER_PHONE_NUMBER);
        assertEquals(searchBookingsRequest.getInitialLastArrivalDate(), INITIAL_LAST_ARRIVAL_DATE);
        assertEquals(searchBookingsRequest.getFinalLastArrivalDate(), FINAL_LAST_ARRIVAL_DATE);
        assertEquals(searchBookingsRequest.getCreditCard(), CREDIT_CARD);
        assertEquals(searchBookingsRequest.getOffsetPage(), OFFSET_PAGE);
        assertEquals(searchBookingsRequest.getMaxNumBookingsByPage(), MAX_NUM_BOOKINGS_BY_PAGE);
        assertEquals(searchBookingsRequest.getWebsiteCode(), WEBSITE_CODE);
        assertEquals(searchBookingsRequest.getBrandCode(), BRAND_CODE);
        assertEquals(searchBookingsRequest.getMembershipId(), MEMBERSHIP_ID);
        assertEquals(searchBookingsRequest.getMemberAccountId(), MEMBER_ACCOUNT_ID);
        assertTrue(Boolean.parseBoolean(searchBookingsRequest.getIsBookingSubscriptionPrime()));
    }
}
