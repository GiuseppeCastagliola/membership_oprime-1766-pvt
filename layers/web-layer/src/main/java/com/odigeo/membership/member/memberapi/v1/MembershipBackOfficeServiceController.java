package com.odigeo.membership.member.memberapi.v1;

import com.edreams.base.DataAccessException;
import com.edreams.base.MissingElementException;
import com.edreams.configuration.ConfigurationEngine;
import com.odigeo.commons.messaging.PublishMessageException;
import com.odigeo.membership.MembershipBackOfficeService;
import com.odigeo.membership.member.BackOfficeService;
import com.odigeo.membership.member.memberapi.util.LogUtils;
import com.odigeo.membership.member.memberapi.v1.util.MembershipExceptionMapper;
import com.odigeo.membership.request.backoffice.WelcomeEmailRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class MembershipBackOfficeServiceController implements MembershipBackOfficeService {

    private static final Logger LOGGER = LoggerFactory.getLogger(MembershipBackOfficeServiceController.class);

    @Override
    public Boolean updateMembershipMarketingInfo(Long membershipId, String email) {
        try {
            getMembershipBackOfficeService().updateMembershipMarketingInfo(membershipId);
            return true;
        } catch (MissingElementException | PublishMessageException e) {
            LogUtils.smallLog(LOGGER, e.getClass().getSimpleName() + " trying to send marketing message", e);
        } catch (DataAccessException e) {
            LogUtils.smallLog(LOGGER, "DataAccessException trying to send marketing message", e);
            throw MembershipExceptionMapper.map(e);
        }
        return false;
    }

    @Override
    public Boolean sendWelcomeEmail(Long membershipId, WelcomeEmailRequest welcomeEmailRequest) {
        boolean result = false;
        try {
            result = getMembershipBackOfficeService().sendWelcomeEmail(membershipId, welcomeEmailRequest.getBookingId());
        } catch (MissingElementException e) {
            LogUtils.smallLog(LOGGER, "MissingElementException at sending welcome email", e);
        } catch (DataAccessException e) {
            LogUtils.smallLog(LOGGER, "DataAccessException at sending welcome email", e);
            throw MembershipExceptionMapper.map(e);
        }
        return result;
    }

    private BackOfficeService getMembershipBackOfficeService() {
        return ConfigurationEngine.getInstance(BackOfficeService.class);
    }
}
