import React, {Component} from 'react';
import Bem from "react-bem-helper";
import {Col, Layout, Row} from 'antd';
import {Footer, Header, Uploader} from 'Components';
import './style.scss'

const { Content } = Layout;
const classes = new Bem('post-booking');

class PostBooking extends Component {
    render() {
        return (
            <Layout>
                <Header text="Prime employees subscription"/>
                <Content {...classes('content')}>
                    <Row>
                        <Col span={18} offset={3}>
                            <Uploader />
                        </Col>
                    </Row>
                </Content>
                <Footer/>
            </Layout>
        );
    }
}

export default PostBooking;
