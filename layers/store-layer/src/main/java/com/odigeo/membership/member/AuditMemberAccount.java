package com.odigeo.membership.member;

import java.util.Objects;

class AuditMemberAccount {

    private final Long memberAccountId;
    private final Long userId;
    private final String name;
    private final String lastName;

    AuditMemberAccount(final Long memberAccountId, final Long userId, final String name, final String lastName) {
        this.memberAccountId = memberAccountId;
        this.userId = userId;
        this.name = name;
        this.lastName = lastName;
    }

    public static AuditMemberAccountBuilder builder() {
        return new AuditMemberAccountBuilder();
    }

    public Long getMemberAccountId() {
        return memberAccountId;
    }

    public Long getUserId() {
        return userId;
    }

    public String getName() {
        return name;
    }

    public String getLastName() {
        return lastName;
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final AuditMemberAccount that = (AuditMemberAccount) o;
        return Objects.equals(memberAccountId, that.memberAccountId)
                && Objects.equals(userId, that.userId)
                && Objects.equals(name, that.name)
                && Objects.equals(lastName, that.lastName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(memberAccountId, userId, name, lastName);
    }

    public static final class AuditMemberAccountBuilder {

        private Long memberAccountId;
        private Long userId;
        private String name;
        private String lastName;

        AuditMemberAccountBuilder() {
        }

        public AuditMemberAccountBuilder memberAccountId(Long memberAccountId) {
            this.memberAccountId = memberAccountId;
            return this;
        }

        public AuditMemberAccountBuilder userId(Long userId) {
            this.userId = userId;
            return this;
        }

        public AuditMemberAccountBuilder name(String name) {
            this.name = name;
            return this;
        }

        public AuditMemberAccountBuilder lastName(String lastName) {
            this.lastName = lastName;
            return this;
        }

        public AuditMemberAccount build() {
            return new AuditMemberAccount(memberAccountId, userId, name, lastName);
        }
    }
}
