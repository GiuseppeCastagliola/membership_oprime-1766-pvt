package com.odigeo.membership.onboarding;

import com.google.inject.Singleton;
import com.odigeo.db.DbUtils;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

@Singleton
public class OnboardingStore {

    private static final String ADD_ONBOARDING_SQL = "INSERT INTO GE_ONBOARDING(ID, MEMBER_ACCOUNT_ID, EVENT, DEVICE) VALUES (?, ?, ?, ?) ";
    private static final String SELECT_NEXT_ONBOARDING_ID = "SELECT SEQ_GE_ONBOARDING_ID.nextval FROM dual ";

    public long createOnboarding(DataSource dataSource, Long memberAccountId, OnboardingEvent event, OnboardingDevice device) throws SQLException {
        try (Connection connection = dataSource.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(ADD_ONBOARDING_SQL)) {
            final long nextId = DbUtils.nextSequence(connection.prepareStatement(SELECT_NEXT_ONBOARDING_ID));
            preparedStatement.setLong(1, nextId);
            preparedStatement.setLong(2, memberAccountId);
            preparedStatement.setString(3, event.toString());
            preparedStatement.setString(4, device.toString());
            preparedStatement.execute();
            return nextId;
        }
    }
}
